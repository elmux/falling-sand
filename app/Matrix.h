#pragma once

#include <SFML/Graphics.hpp>
#include "Global.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <functional>
#include <cmath>

#include "Cell.h"
#include "Empty.h"
#include "Solid.h"
#include "Liquid.h"
#include "Gas.h"
#include "GOL.h"

class Matrix
{
private:
	sf::RenderWindow& window;
	sf::Image img;
	sf::Texture texture;
	sf::Sprite sprite;

public:
	static const int scale = SCALE;
	static const int width = (WIDTH / scale);
	static const int height = (HEIGHT / scale);

	Cell** matrix;

	sf::Vector2f pos;
	sf::CircleShape brush;

	int ticknum = 0;
	bool paused = false;
	bool needsUpdate = false;
	int selected = 1;
	int thickness = 5;

	Matrix(sf::RenderWindow& window_);

	void tick();

	void iterateAndApplyMethodBetweenTwoPoints
	(int x1, int y1, int x2, int y2, std::function<int(int, int)> func);

	bool withinBounds(int x, int y);

	bool isEmpty(int x, int y);

	bool isMoreDense(Cell& c1, int x, int y);

	void switchCells(int x1, int y1, int x2, int y2);

	void setCell(int x, int y, int id);

	void paint(int x, int y, int id);

	int countNeighbors(int x, int y);

	void killCell(int x, int y);

	void clear();

	void randomize();

	void update();

	void render();
};
