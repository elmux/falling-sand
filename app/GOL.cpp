#include "GOL.h"

GOL::GOL() : Cell()
{

}

GOL::GOL(bool s) : Cell()
{
	r = 0;
	g = 0;
	b = 0;
	state = s;
	if (state == 1)
		r = 255;
	maxSpeed = 0;
	acc = 0;
	vel = 0;
	density = 1.f;
}

void GOL::tick(Matrix* m, int& x, int& y)
{
	update(m, x, y);
}

int GOL::update(Matrix* m, int& x, int& y)
{
	int neighbors = m->countNeighbors(x, y);
	if (state == 1 && neighbors < 2) {
		kill();
	}
	else if (state == 1 && neighbors > 3) {
		kill();
	}
	else if (state == 0 && neighbors == 3) {
		revive();
	}

	return x + y * m->width;
}

void GOL::kill()
{
	state = 0;
	r = g = b = 0;
}

void GOL::revive()
{
	state = 1;
	r = 255;
}
