#pragma once
#include "Cell.h"

class GOL : public Cell
{
public:

	GOL();

	GOL(bool s);

	void tick(Matrix* m, int& x, int& y);

	int update(Matrix* m, int& x, int& y);

	void kill();

	void revive();
};

class ProbGOL : public GOL
{
public:
	float prob;

	ProbGOL(bool s);

	void tick(Matrix* m, int& x, int& y);

	int update(Matrix* m, int& x, int& y);

	void kill();

	void revive();
};