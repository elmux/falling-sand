#include "Cell.h"

Cell::Cell() 
{

};

void Cell::tick(Matrix* m, int& x, int& y)
{
	updateVelocity();
	if (!modified) return;
	int i = x;
	int j = y;
	int uCount = getUpdateCount();
	for (int u = 0; u < uCount; u++) {
		int newIndex = update(m, i, j);
		if (newIndex != i + j * m->width) {
			i = newIndex % m->width;
			j = newIndex / m->width;
		}
		else {
			m->matrix[i + j * m->width]->resetVelocity();
			break;
		}
	}
}

bool Cell::moveUp(Matrix* m, int& x, int& y)
{
	if (m->isMoreDense(*this, x, y - 1)) {
		m->switchCells(x, y, x, y - 1);
		return true;
	}
	return false;
}

bool Cell::moveDown(Matrix* m, int& x, int& y)
{
	if (m->isMoreDense(*this, x, y + 1)) {
		m->switchCells(x, y, x, y + 1);
		return true;
	}
	return false;
}

bool Cell::moveSide(Matrix* m, int& x, int& y)
{
	int xx = x;
	if (m->isMoreDense(*this, x + 1, y) && m->isMoreDense(*this, x - 1, y)) {
		if (rand() % 2 == 1)
			xx = x + 1;
		else
			xx = x - 1;
	}
	else if (m->isMoreDense(*this, x + 1, y))
		xx = x + 1;
	else if (m->isMoreDense(*this, x - 1, y))
		xx = x - 1;
	else
		return x + y * m->width;
	m->switchCells(x, y, xx, y);
	return xx + y * m->width;
}

int Cell::moveSideUp(Matrix* m, int& x, int& y)
{
	int xx = x; int yy = y;
	if (m->isMoreDense(*this, x, y - 1))
		yy = y - 1;
	else if (m->isMoreDense(*this, x + 1, y) && m->isMoreDense(*this, x - 1, y)) {
		if (rand() % 2 == 1)
			xx = x + 1;
		else
			xx = x - 1;
	}
	else if (m->isMoreDense(*this, x + 1, y))
		xx = x + 1;
	else if (m->isMoreDense(*this, x - 1, y))
		xx = x - 1;
	else
		return x + y * m->width;
	m->switchCells(x, y, xx, yy);
	return xx + yy * m->width;
}

int Cell::moveSideDown(Matrix* m, int& x, int& y)
{
	int xx = x; int yy = y;
	if (m->isMoreDense(*this, x, y + 1))
		yy = y + 1;
	else if (m->isMoreDense(*this, x + 1, y) && m->isMoreDense(*this, x - 1, y)) {
		if (rand() % 2 == 1)
			xx = x + 1;
		else
			xx = x - 1;
	}
	else if (m->isMoreDense(*this, x + 1, y))
		xx = x + 1;
	else if (m->isMoreDense(*this, x - 1, y))
		xx = x - 1;
	else
		return x + y * m->width;
	m->switchCells(x, y, xx, yy);
	return xx + yy * m->width;
}

int Cell::moveDiagUp(Matrix* m, int& x, int& y)
{
	int xx = x; int yy = y;
	if (m->isMoreDense(*this, x, y - 1))
		yy = y - 1;
	else if (m->isMoreDense(*this, x + 1, y) && m->isMoreDense(*this, x - 1, y)) {
		if (rand() % 2 == 1)
			xx = x + 1;
		else
			xx = x - 1;
	}
	else if (m->isMoreDense(*this, x + 1, y))
		xx = x + 1;
	else if (m->isMoreDense(*this, x - 1, y))
		xx = x - 1;
	else
		return x + y * m->width;
	m->switchCells(x, y, xx, yy);
	return xx + yy * m->width;
}

int Cell::moveDiagDown(Matrix* m, int& x, int& y)
{
	int xx = x; int yy = y;
	if (m->isMoreDense(*this, x, y + 1))
		yy = y + 1;
	else if (m->isMoreDense(*this, x + 1, y + 1) && m->isMoreDense(*this, x - 1, y + 1)) {
		yy = y + 1;
		if (rand() % 2 == 1)
			xx = x + 1;
		else
			xx = x - 1;
	}
	else if (m->isMoreDense(*this, x + 1, y + 1)) {
		xx = x + 1; yy = y + 1;
	}
	else if (m->isMoreDense(*this, x - 1, y + 1)) {
		xx = x - 1; yy = y + 1;
	}
	else
		return x + y * m->width;
	m->switchCells(x, y, xx, yy);
	return xx + yy * m->width;
}

int Cell::moveEveryDir(Matrix* m, int& x, int& y)
{
	int xx = x; int yy = y;
	int ran = rand() % 8;
	if (ran == 0)
		if (m->isMoreDense(*this, x + 1, y + 1)) {
			xx = x + 1; yy = y + 1;
		}
	else if (ran == 1)
		if (m->isMoreDense(*this, x-1, y-1)) {
			xx = x - 1; yy = y - 1;
		}
	else if (ran == 2)
		if (m->isMoreDense(*this, x-1, y+1)) {
			xx = x - 1; yy = y + 1;
		}
	else if (ran == 3)
		if (m->isMoreDense(*this, x+1, y-1)) {
			xx = x + 1; yy = y - 1;
		}
	else if (ran == 4)
		if (m->isMoreDense(*this, x, y-1)) {
			yy = y - 1;
		}
	else if (ran == 5)
		if (m->isMoreDense(*this, x, y+1)) {
			yy = y + 1;
		}
	else if (ran == 6)
		if (m->isMoreDense(*this, x+1, y)) {
			xx = x + 1;
		}
	else if (ran == 7)
		if (m->isMoreDense(*this, x-1, y)) {
			xx = x - 1;
		}
	m->switchCells(x, y, xx, yy);
	return xx + yy * m->width;
}

void Cell::updateVelocity()
{
	if (maxSpeed != 0) {
		vel += acc;
		if (vel >= maxSpeed) vel = maxSpeed;
	}
	modified = vel != 0;
}

void Cell::resetVelocity()
{
	vel = 0;
}

int Cell::getUpdateCount()
{
	float a = abs(vel);
	int f = floor(a);
	float m = a - f;
	int ret = f + (randFloat(0.0, 1.0) < m ? 1 : 0);
	return ret;
}

void Cell::kill()
{
	state = 0;
}

void Cell::revive()
{
	state = 1;
}

Cell::~Cell() {};
