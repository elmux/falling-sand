#include <SFML/Graphics.hpp>
#include "Global.h"
#include "Matrix.h"
using namespace std;

string getCellName(int id)
{
	switch (id)
	{
	case 0: return "blank";  break;
	case 1: return "sand";  break;
	case 2: return "water";  break;
	case 3: return "smoke";  break;
	case 4: return "gas";  break;
	case 5: return "snow";  break;
	case 6: return "fire";  break;
	case 7: return "lava";  break;
	case 8: return "wall";  break;
	case 9: return "game of life";  break;
	case 10: return "game of probability";  break;
	}
	return "blank";
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "", sf::Style::Close | sf::Style::Resize);
	window.setFramerateLimit(144);

	float deltaTime = 0.0f;
	float frameTime = 1.0f / 60.0f;
	sf::Clock deltaClock;
	float elapsedTime = 0.0f;

	Matrix* m = new Matrix(window);

	while (window.isOpen())
	{
		elapsedTime = deltaClock.restart().asSeconds();
		window.setTitle(std::to_string((int)(1.f / elapsedTime)) + " FPS");
		deltaTime += elapsedTime;
		if (deltaTime >= frameTime)
		{
			deltaTime -= frameTime;
			elapsedTime = frameTime;
			sf::Event event;
			while (window.pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::Resized:
					break;
				case sf::Event::MouseWheelMoved:
					event.mouseWheel.delta > 0 ? m->thickness++ :
						(m->thickness > 0 ? m->thickness-- : m->thickness = 0);
					print("Thickness " + std::to_string(m->thickness));
					break;
				case sf::Event::MouseButtonReleased:
					if (event.key.code == sf::Mouse::XButton1) {
						m->selected > 1 ? m->selected-- : m->selected = 1;
						print("Cell " + getCellName(m->selected) + " selected");
					}
					if (event.key.code == sf::Mouse::XButton2) {
						m->selected++;
						print("Cell " + getCellName(m->selected) + " selected");
					}
				case sf::Event::KeyReleased:
					if (event.key.code == sf::Keyboard::R)
						m->randomize();
					if (event.key.code == sf::Keyboard::C)
						m->clear();
					if (event.key.code == sf::Keyboard::Right) {
						m->selected++;
						print("Cell " + getCellName(m->selected) + " selected");
					}
					if (event.key.code == sf::Keyboard::Left) {
						m->selected > 1 ? m->selected-- : m->selected = 1;
						print("Cell " + getCellName(m->selected) + " selected");
					}
					if (event.key.code == sf::Keyboard::Space) {
						m->paused = !m->paused;
						m->paused ? print("Paused") : print("Unpaused");
					}
					if (event.key.code == sf::Keyboard::Up)
						frameTime /= 1.5;
					if (event.key.code == sf::Keyboard::Down)
						frameTime *= 1.5;
					break;
				}
			}

			m->tick();

		}

	}
	return 0;
}
