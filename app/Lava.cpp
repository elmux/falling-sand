#include "Liquid.h"

Lava::Lava() : Liquid()
{
	state = 1;
	r = 255;
	g = rand() % 256;
	b = 0;
	maxSpeed = 2;
	acc = 0.05f;
	density = 0.5f;
}

int Lava::update(Matrix* m, int& x, int& y)
{
	if (rand() % 200 == 100) {
		delete m->matrix[x + y * m->width];
		m->matrix[x + y * m->width] = new Smoke();
		return x + y * m->width;
	}
	else {
		g++;
		return moveSideDown(m, x, y);
	}
}
