#include "Matrix.h"

Matrix::Matrix(sf::RenderWindow& window_) : window(window_)
{
	matrix = new Cell* [width * height];

	brush.setOutlineColor(sf::Color::Red);
	brush.setFillColor(sf::Color(0, 0, 0, 0));
	brush.setOutlineThickness(SCALE / 2);
	sprite.setPosition(0, 0);
	sprite.setScale(sf::Vector2f(scale, scale));
	img.create(width, height, sf::Color::Black);

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++)
			matrix[x + y * width] = new Empty();

	window.clear();
	render();
	window.display();
}

void Matrix::tick()
{
	needsUpdate = false;
	pos = window.mapPixelToCoords(sf::Mouse::getPosition(window));

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		paint(pos.x / SCALE, pos.y / SCALE, selected);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
		paint(pos.x / SCALE, pos.y / SCALE, 0);
	}

	if (!paused)
		update();
	window.clear();
	render();
	window.display();
}

void Matrix::killCell(int x, int y)
{
	delete matrix[x + y * width];
	matrix[x + y * width] = new Empty();
}

bool Matrix::withinBounds(int x, int y)
{
	return !(x < 0 || y < 0 || x >= width || y >= height);
}

bool Matrix::isMoreDense(Cell& c1, int x, int y)
{
	if (!withinBounds(x, y)) return false;
	return c1.density > matrix[x + y * width]->density ? 1 : 0;
}

bool Matrix::isEmpty(int x, int y)
{
	if (!withinBounds(x, y)) return false;
	if (matrix[x + y * width]->density == 0) return true;
	return false;
}

void Matrix::switchCells(int x1, int y1, int x2, int y2)
{
	Cell* tempPtr = matrix[x1 + y1 * width];
	matrix[x1 + y1 * width] = matrix[x2 + y2 * width];
	matrix[x2 + y2 * width] = tempPtr;
}

void Matrix::setCell(int x, int y, int id)
{
	if (withinBounds(x, y)) {
		delete matrix[x + y * width];
		switch (id)
		{
		case 1:
			matrix[x + y * width] = new Sand(); break;
		case 2:						 
			matrix[x + y * width] = new Water(); break;
		case 3:
			matrix[x + y * width] = new Smoke(); break;
		case 4:						 
			matrix[x + y * width] = new Toxic(); break;
		case 5:						 
			matrix[x + y * width] = new Snow(); break;
		case 6:
			matrix[x + y * width] = new Fire(); break;
		case 7:
			matrix[x + y * width] = new Lava(); break;
		case 8:
			matrix[x + y * width] = new Wall(x, y); break;
		case 9:						 
			matrix[x + y * width] = new GOL(true); break;
		case 10:						 
			matrix[x + y * width] = new ProbGOL(true); break;
		default:					 
			matrix[x + y * width] = new Empty(); break;
		}
	}
}

void Matrix::paint(int x, int y, int id)
{
	int radiusSquared = thickness * thickness;
	for (int y1 = -thickness; y1 <= thickness; y1++) {
		for (int x1 = -thickness; x1 <= thickness; x1++) {
			if (x1 * x1 + y1 * y1 <= radiusSquared && randFloat() < 0.75) {
				setCell(x + x1, y + y1, id);
			}
		}
	}
}

int Matrix::countNeighbors(int x, int y)
{
	int neighbors = 0;
	if (matrix[(x - 1) + (y - 1) * width]	->state == 1)	neighbors++;
	if (matrix[ x	   + (y - 1) * width]	->state == 1)	neighbors++;
	if (matrix[(x + 1) + (y - 1) * width]	->state == 1)	neighbors++;
	if (matrix[(x - 1) +  y		 * width]	->state == 1)	neighbors++;
	if (matrix[(x + 1) +  y		 * width]	->state == 1)	neighbors++;
	if (matrix[(x - 1) + (y + 1) * width]	->state == 1)	neighbors++;
	if (matrix[ x	   + (y + 1) * width]	->state == 1)	neighbors++;
	if (matrix[(x + 1) + (y + 1) * width]	->state == 1)	neighbors++;
	return neighbors;
}

void Matrix::clear()
{
	needsUpdate = true;
	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++) {
			delete matrix[x + y * width];
			matrix[x + y * width] = new Empty();
		}
	print("Matrix cleared");
}

void Matrix::randomize()
{
	needsUpdate = true;
	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++) {
			if (randFloat() < 0.75f)
				setCell(x, y, rand() % 9);
			else
				matrix[x + y * width] = new Empty();
		}
	print("Matrix randomized");
}

void Matrix::update()
{
	ticknum++;
	for (int y = height - 2; y > 0; y--) {
		int iterDir = rand() % 2;
		for (int xx = 1; xx < width - 1; xx++) {
			int x = iterDir ? xx : -xx - 1 + width;
			if (matrix[x + y * width]->ticknum == ticknum) continue;
			matrix[x + y * width]->ticknum = ticknum;
			matrix[x + y * width]->tick(this, x, y);
		}
	}
}

void Matrix::render()
{
	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++) {
			if (matrix[x + y * width]->state > 0) {
				img.setPixel(x, y, sf::Color(
					matrix[x + y * width]->r,
					matrix[x + y * width]->g, 
					matrix[x + y * width]->b,
					matrix[x + y * width]->lifetime));
			}
			else
				img.setPixel(x, y, sf::Color::Black);
		}

	texture.loadFromImage(img);
	sprite.setTexture(texture, true);
	window.draw(sprite);
	brush.setRadius(thickness * SCALE);
	brush.setPosition(sf::Vector2f((pos.x - thickness * SCALE) + 2,
								   (pos.y - thickness * SCALE) + 2));
	window.draw(brush);
}
