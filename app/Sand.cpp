#include "Solid.h"

Sand::Sand() : Solid()
{
	state = 1;
	int ran = rand() % 63;
	r = 255 - ran;
	g = 255 - ran;
	b = 127;
	density = 0.75f;
}

int Sand::update(Matrix* m, int& x, int& y)
{
	return moveDiagDown(m, x, y);
}
