#pragma once
#include "Cell.h"

class Solid : public Cell
{
public:
	Solid(){};
};

class Wall : public Solid
{
public:

	Wall(int& x, int& y);

	void tick(Matrix* m, int& x, int& y);

	int update(Matrix* m, int& x, int& y);

};

class Sand : public Solid
{
public:

	Sand();

	int update(Matrix* m, int& x, int& y);

};

class Snow : public Solid
{
public:

	Snow();

	int update(Matrix* m, int& x, int& y);

};

class Fire : public Solid
{
public:

	Fire();

	void tick(Matrix* m, int& x, int& y);

	int update(Matrix* m, int& x, int& y);

};