#include "Solid.h"

Wall::Wall(int& x, int& y) : Solid()
{
	state = 1;
	r = 0;
	if (y % 2 == 0)
		if (x % 2 == 0)
			g = b = 255;
		else
			g = b = 127;
	else
		if (x % 2 == 0)
			g = b = 127;
		else
			g = b = 255;
	density = 1.f;
}

void Wall::tick(Matrix* m, int& x, int& y)
{
	return;
}

int Wall::update(Matrix* m, int& x, int& y)
{
	return 0;
}
