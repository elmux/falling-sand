#pragma once

class Matrix;

class Cell
{
public:
	int r;
	int g;
	int b;
	int state;
	int ticknum;

	float density = 0.f;
	int lifetime = 255;

	float maxSpeed = 8.0f;
	float acc = 0.4f;
	float vel = 0.0f;
	bool modified = false;

	Cell();

	virtual bool moveUp(Matrix* m, int& x, int& y);
	virtual bool moveDown(Matrix* m, int& x, int& y);
	virtual bool moveSide(Matrix* m, int& x, int& y);
	virtual int moveSideUp(Matrix* m, int& x, int& y);
	virtual int moveSideDown(Matrix* m, int& x, int& y);
	virtual int moveDiagUp(Matrix* m, int& x, int& y);
	virtual int moveDiagDown(Matrix* m, int& x, int& y);
	virtual int moveEveryDir(Matrix* m, int& x, int& y);

	virtual int update(Matrix* m, int& x, int& y) = 0;

	virtual void tick(Matrix* m, int& x, int& y);

	virtual void updateVelocity();

	virtual void resetVelocity();

	virtual int getUpdateCount();

	virtual void kill();

	virtual void revive();

	~Cell();
};

#include "GOL.h"
#include "ProbGOL.h"