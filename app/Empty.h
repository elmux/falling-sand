#pragma once
#include "Cell.h"

class Empty : public Cell
{
public:
	Empty();

	void tick(Matrix* m, int& x, int& y);

	int update(Matrix* m, int& x, int& y);
};

