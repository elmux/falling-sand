#pragma once
#include <string>
#define WIDTH 1200
#define HEIGHT 900
#define SCALE 4

void	print(std::string str);
int		randInt(int min, int max);
float	randFloat(float a, float b);
float	randFloat();