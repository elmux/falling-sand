#include "Solid.h"

Fire::Fire() : Solid()
{
	state = 1;
	r = 255;
	g = rand()%191;
	b = 0;
	maxSpeed = 1;
	acc = 0.5f;
	density = 0.5f;
}

void Fire::tick(Matrix* m, int& x, int& y)
{
	if (rand() % 100 == 1) {
		delete m->matrix[x + y * m->width];
		m->matrix[x + y * m->width] = new Smoke();
	}
	else {
		++g > 191 ? g = 0 : g;
	}
}

int Fire::update(Matrix* m, int& x, int& y)
{
	if (rand() % 100 == 1) {
		delete m->matrix[x + y * m->width];
		m->matrix[x + y * m->width] = new Smoke();
		return x + y * m->width;
	}
	else {
		g++;
		return moveEveryDir(m, x, y);
	}
}
