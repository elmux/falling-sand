#pragma once
#include "Cell.h"

class Gas : public Cell
{
public:
	Gas(){};
};

class Smoke : public Gas
{
public:

	Smoke();

	int update(Matrix* m, int& x, int& y);

};

class Toxic : public Gas
{
public:

	Toxic();

	int update(Matrix* m, int& x, int& y);

};