#include "Liquid.h"

Water::Water() : Liquid()
{
	state = 1;
	r = 0;
	g = 0;
	b = 255 - rand()% 127;
	maxSpeed = 16.0;
	acc = 0.8;
	vel = 0.0;
	density = 0.50f;
}

int Water::update(Matrix* m, int& x, int& y)
{
	return moveSideDown(m, x, y);
}
