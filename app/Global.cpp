#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

void print(std::string str)
{
    std::cout << str << std::endl;
}

int randInt(int min, int max)
{
    return rand() % (max - min + 1) + min;
}

float randFloat(float a, float b)
{
    float random = ((float)rand()) / (float)RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

float randFloat()
{
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}