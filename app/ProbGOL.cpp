#include "Matrix.h"

ProbGOL::ProbGOL(bool s) : GOL()
{
	prob = randFloat(0.5, 1.0);
	r = 0;
	g = 0;
	b = 0;
	state = s;
	if (state == 1)
		g = 255;
	density = 1.f;
}

void ProbGOL::tick(Matrix* m, int& x, int& y)
{
	update(m, x, y);
}

int ProbGOL::update(Matrix* m, int& x, int& y)
{
	if (randFloat() < prob) {
		int neighbors = m->countNeighbors(x, y);
		if ((m->matrix[x + y * m->width]->state == 1) && (neighbors < 2)) {
			m->matrix[x + y * m->width]->kill();
		}
		else if ((m->matrix[x + y * m->width]->state == 1) && (neighbors > 3)) {
			m->matrix[x + y * m->width]->kill();
		}
		else if ((m->matrix[x + y * m->width]->state == 0) && (neighbors == 3)) {
			m->matrix[x + y * m->width]->revive();
		}
	}

	return 0;
}

void ProbGOL::kill()
{
	if (randFloat() < prob) {
		state = 0;
		r = g = b = 0;
	}
}

void ProbGOL::revive()
{
	if (randFloat() < prob) {
		state = 1;
		g = 255;
	}
}