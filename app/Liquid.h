#pragma once
#include "Cell.h"

class Liquid : public Cell
{
public:
	Liquid(){};
};

class Water : public Liquid
{
public:

	Water();

	int update(Matrix* m, int& x, int& y);

};

class Lava : public Liquid
{
public:

	Lava();

	int update(Matrix* m, int& x, int& y);

};
