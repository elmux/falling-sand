#include "Solid.h"

Snow::Snow() : Solid()
{
	state = 1;
	int ran = rand() % 63;
	r = 255 - ran;
	g = 255 - ran;
	b = 255 - ran;
	maxSpeed = 1;
	acc = 0.1f;
	density = 0.75f;
}

int Snow::update(Matrix* m, int& x, int& y)
{
	if (rand() % 2 == 1) {
		if (rand() % 2 == 1)
			return moveDiagDown(m, x, y);
		else
			return moveSide(m, x, y);
	}
	return x + y * m->width;
}
