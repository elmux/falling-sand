#include "Gas.h"

Toxic::Toxic() : Gas()
{
	state = 1;
	int ran = rand() % 127;
	r = 0;
	g = 255 - ran;
	b = 0;
	maxSpeed = 1;
	acc = 0.1f;
	density = 0.15f;
}

int Toxic::update(Matrix* m, int& x, int& y)
{
	if (lifetime <= 0) m->killCell(x, y);
	else {
		lifetime - 1 >= 0 ? lifetime -= 1 : lifetime = 0;
		if (rand() % 2 == 1) {
			if (rand() % 2 == 1)
				return moveDiagUp(m, x, y);
			else
				return moveSide(m, x, y);
		}
		return x + y * m->width;
	}
}
