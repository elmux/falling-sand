#include "Gas.h"

Smoke::Smoke() : Gas()
{
	state = 1;
	int ran = rand() % 63;
	r = 127 - ran;
	g = 127 - ran;
	b = 127 - ran;
	maxSpeed = 0.5;
	acc = 0.5;
	vel = 0.0;
	density = 0.25f;
}

int Smoke::update(Matrix* m, int& x, int& y)
{
	if (lifetime <= 0) m->killCell(x, y);
	else {
		lifetime-1 >= 0 ? lifetime -= 1 : lifetime = 0;
		return moveSideUp(m, x, y);
	}
}
