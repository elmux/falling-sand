# Falling Sand

Cellular automaton/game of life/falling sand simulator made with SFML & C++.

![Screen Capture](screencapture.gif)
